package com.apachekarkamongodb.apachekarkamongodb.model;

import com.apachekarkamongodb.apachekarkamongodb.config.EventType;
import lombok.*;

import java.time.Instant;
import java.time.LocalDateTime;

@Setter
@Getter
@NoArgsConstructor
@AllArgsConstructor
public class ProductEvent {
	private String productId;
	private String eventType;
	private String timestamp = LocalDateTime.now().toString();
}

