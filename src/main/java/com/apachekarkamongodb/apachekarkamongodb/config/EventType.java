package com.apachekarkamongodb.apachekarkamongodb.config;

public enum EventType {
	PRODUCT_VIEWED,
	PRODUCT_ADDED_TO_CART,
	PRODUCT_PURCHASED
}
