package com.apachekarkamongodb.apachekarkamongodb.controller;

import com.apachekarkamongodb.apachekarkamongodb.model.ProductEvent;
import com.apachekarkamongodb.apachekarkamongodb.service.ProductEventProducerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RequestMapping("/v1/products")
@RestController
public class ProductEventController {
	@Autowired
	private ProductEventProducerService producerService;

	@PostMapping
	public ResponseEntity<Boolean> createProductEvent(@RequestBody ProductEvent event) {
		System.out.println("asdasd");
		producerService.send(event);
		return ResponseEntity.ok().body(true);
	}


}
