package com.apachekarkamongodb.apachekarkamongodb.service;

import com.apachekarkamongodb.apachekarkamongodb.model.ProductEvent;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class ProductEventProducerService {
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;

	@Value("${topic.name.producer}")
	private String topicName;

	public void send(ProductEvent productEvent) {
		try {
			log.info("Payload enviado: {}", productEvent);
			ObjectMapper objectMapper = new ObjectMapper();
			String payload = objectMapper.writeValueAsString(productEvent);
			kafkaTemplate.send(topicName, payload);

		}catch (JsonProcessingException e) {
			log.info("ERRORRRRRRRRRRR {}",e.getMessage());
		}
	}
}
